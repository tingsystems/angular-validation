(function () {
    'use strict';

    function Routes($stateProvider, $urlRouterProvider, $locationProvider) {

        $stateProvider
            .state('index', {
                url: '/',
                abstract: true,
                templateUrl: '/static/app/index.html',
                controllerAs: 'Home',
                controller: 'HomeCtrl'
            })
            .state('home', {
                url: '/',
                views: {
                    'content': {
                        templateUrl: '/templates/home.html',
                        controllerAs: 'Home',
                        controller: 'HomeCtrl'
                    }
                }
            });

        $urlRouterProvider.otherwise('/');
        $locationProvider.html5Mode(true);
    }

    function AppConfig() {
    }

    function Run() {
    }

    angular.module('annalise', ['ui.router', 'ts.controllers', 'ngSanitize', 'app.templates',
        'ngAnimate', 'ui.bootstrap', 'ui.select', 'ngMessages'])
        .config(Routes)
        .config(AppConfig)
        .run(Run);

    Run.$inject = [];
    Routes.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];
    AppConfig.$inject = [];
})();
